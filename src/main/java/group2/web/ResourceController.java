package group2.web;

import group2.service.ResourceService;
import org.hyperic.sigar.SigarException;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by qian on 2017/4/19.
 */
@RestController
@EnableAutoConfiguration
public class ResourceController {

    @RequestMapping("/")
    public void tmp() throws SigarException {
        return ResourceService.printCpuInfo();
    }
}
