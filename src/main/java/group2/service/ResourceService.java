package group2.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hyperic.sigar.CpuInfo;
import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by qian on 2017/4/19.
 */
@Service("ResourceService")
public class ResourceService {

    private static Sigar sigar = null;

    public static Sigar getSigarInstance(){
        if(sigar == null){
            try {
                String path = System.getProperty("java.library.path");
                System.setProperty("java.library.path", path+";E:\\project\\新人演习\\lib\\hyperic-sigar-1.6.4\\sigar-bin\\lib");
            return new Sigar();
        } catch (Exception e) {
            return null;
        }
        }else{
            return sigar;
        }
    }

    //获取cpu信息
    public static String printCpuInfo() throws SigarException {
        Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>();
        Sigar sigar = getSigarInstance();
//		StringBuffer str = new StringBuffer();
        CpuInfo[] cpuInfoArray = sigar.getCpuInfoList();
        CpuPerc[] cpuPercArray = sigar.getCpuPercList();
        DecimalFormat decimalFormat = new DecimalFormat(".000");
//		str.append("cpu数量："+cpuInfoArray.length+"\n");
        for (int i = 0; i < cpuInfoArray.length; i++) {
            Map<String, Object> cpuMap = new HashMap<String, Object>();

//			str.append("cpu "+(i+1)+":\n");
//			str.append("型号："+cpuInfoArray[i].getModel()+"\n");
//			str.append("频率："+cpuInfoArray[i].getMhz()+"\n");
//			str.append("系统使用率："+Double.parseDouble(decimalFormat.format(cpuPercArray[i].getSys()))+"\n");
//			str.append("用户使用率："+Double.parseDouble(decimalFormat.format(cpuPercArray[i].getUser()))+"\n");
//			cpuMap.put("str", str);

            cpuMap.put("model", cpuInfoArray[i].getModel());
            cpuMap.put("frequency", cpuInfoArray[i].getMhz());
            cpuMap.put("sysper", Double.parseDouble(decimalFormat.format(cpuPercArray[i].getSys())));
            cpuMap.put("userper", Double.parseDouble(decimalFormat.format(cpuPercArray[i].getUser())));
            map.put("cpu" + (i + 1), cpuMap);
        }
//		System.out.println(str.toString());
        for(String key: map.keySet()){
            System.out.println(key+":");
            Map<String, Object> cpuMap = map.get(key);
            for(String cpuKey: cpuMap.keySet()){
                System.out.println(cpuKey+": "+cpuMap.get(cpuKey));
            }
        }

        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }


    @Resource(name = "sigar")
    public void setSigar(Sigar sigar) {
        this.sigar = sigar;
    }

}
